<?php

/**
 * @file
 * Place holder for the Activity Stream module.
 */

/**
 * Implements hook_token_info().
 *
 * We need this part until the token module will be exported to Drupal 8.
 */
function user_activity_stream_token_info() {
  $type = [
    'name' => t('User Activity Stream'),
    'description' => t('Tokens for the User Activity Stream module.'),
    'needs-data' => 'message',
  ];

  // User relate tokens.
  $message['author-render'] = [
    'name' => t("Rendered author"),
    'description' => t("The related author rendered to HTML."),
  ];

  // Store related tokens.
  $message['store-render'] = [
    'name' => t("Rendered store"),
    'description' => t("The related store rendered to HTML."),
  ];

  // Product related tokens.
  $message['product-render'] = [
    'name' => t("Rendered product"),
    'description' => t("The related product rendered to HTML."),
  ];

  // Group related tokens.
  $message['group-render'] = [
    'name' => t("Rendered group"),
    'description' => t("The related group rendered to HTML."),
  ];

  // Node related tokens.
  $message['node-render'] = [
    'name' => t("Rendered node"),
    'description' => t("The related node rendered to HTML."),
  ];

  // Comment relate tokens.
  $message['comment-render'] = [
    'name' => t("Rendered comment"),
    'description' => t("The related comment rendered to HTML."),
  ];

  return [
    'types' => ['message' => $type],
    'tokens' => [
      'message' => $message,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function user_activity_stream_tokens($type, $tokens, array $data = [], array $options = []) {
  $token_service = \Drupal::token();

  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = NULL;
  }
  $sanitize = !empty($options['sanitize']);

  $replacements = [];

  if ($type == 'message' && !empty($data['message'])) {
    /** @var \Drupal\message\Entity\Message $message */
    $message = $data['message'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values on the comment.
        case 'author-render':
          $entity = $message->getOwner();
          $view_builder = \Drupal::entityTypeManager()->getViewBuilder('user');
          $message_view = $view_builder->view($entity, 'message_activity_stream');
          $replacements[$original] = \Drupal::service('renderer')->renderRoot($message_view);
          break;

        case 'store-render':
          if ($message->bundle() == 'mas_create_store') {
            $entity = $message->field_store_reference->get(0)->entity;
          }

          $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId());
          $message_view = $view_builder->view($entity, 'message_activity_stream');
          $replacements[$original] = \Drupal::service('renderer')->renderRoot($message_view);
          break;

        case 'product-render':
          if ($message->bundle() == 'mas_create_product') {
            $entity = $message->field_product_reference->get(0)->entity;
          }

          $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId());
          $message_view = $view_builder->view($entity, 'message_activity_stream');
          $replacements[$original] = \Drupal::service('renderer')->renderRoot($message_view);
          break;

        case 'group-render':
          if ($message->bundle() == 'mas_create_group') {
            $entity = $message->field_group_reference->get(0)->entity;
          }

          $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId());
          $message_view = $view_builder->view($entity, 'message_activity_stream');
          $replacements[$original] = \Drupal::service('renderer')->renderRoot($message_view);
          break;

        case 'node-render':
          if ($message->bundle() == 'mas_create_node') {
            $entity = $message->field_node_reference->get(0)->entity;
          }

          $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId());
          $message_view = $view_builder->view($entity, 'message_activity_stream');
          $replacements[$original] = \Drupal::service('renderer')->renderRoot($message_view);
          break;

        case 'comment-render':
          if ($message->bundle() == 'mas_create_comment') {
            $entity = $message->field_comment_reference->get(0)->entity;
          }

          $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId());
          $message_view = $view_builder->view($entity, 'message_activity_stream');
          $replacements[$original] = \Drupal::service('renderer')->renderRoot($message_view);
          break;
      }
    }
  }

  return $replacements;
}