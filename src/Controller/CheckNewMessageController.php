<?php

namespace Drupal\user_activity_stream\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserDataInterface;
use Drupal\views\Views;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;

/**
 * Class CheckNewMessageController.
 */
class CheckNewMessageController extends ControllerBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Creates an CheckNewMessageController object.
   *
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\user\UserDataInterface $userData
   *   The user data service.
   */
  public function __construct(AccountInterface $currentUser, UserDataInterface $userData) {
    $this->currentUser = $currentUser;
    $this->userData = $userData;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('user.data')
    );
  }

  /**
   * Check if a new message arrived and return an Ajax response.
   *
   * Check if the total rows of the view has increased,
   * and if yes then then return Ajax command
   * for adding 'd-block' CSS class to the logo.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   InvokeCommand Ajax command or an empty Ajax response.
   */
  public function checkNewMessage() {
    $response = new AjaxResponse();

    // Get the total rows of the view.
    $view = Views::getView('message_activity_stream_timeline_private');
    $view->setDisplay('block_1');
    $view->execute();
    $total_rows = $view->total_rows;

    if ($total_rows == 0) {
      return $this->responseInvokeCommand('removeClass');
    }

    $uid = $this->currentUser->id();
    $user_data = $this->userData;
    // Get the stored value of total rows of the view.
    $stored_total_rows = $user_data->get('nmn', $uid, 'total_rows');

    // If a user reload the front page, then stop the animation.
    if (is_null($stored_total_rows)) {
      $user_data->set('nmn', $uid, 'total_rows', $total_rows);
      return $this->responseInvokeCommand('removeClass');
    }

    // If the value of total rows was not stored yet, then store it.
    // and add 'd-block' CSS class.
    if (!isset($stored_total_rows)) {
      // Store the current value of the total rows.
      $user_data->set('nmn', $uid, 'total_rows', $total_rows);
      return $this->responseInvokeCommand('addClass');
    }

    // If some rows was removed from the view, then
    // store the current value of total rows.
    if ($total_rows < $stored_total_rows) {
      $user_data->set('nmn', $uid, 'total_rows', $total_rows);
      return $response;
    }

    // Compare the stored value of the total rows with the current.
    // If the total rows of the view has increased, then this
    // is indicate then a new message arrived.
    if ($total_rows > $stored_total_rows) {
      return $this->responseInvokeCommand('addClass');
    }

    if ($total_rows == $stored_total_rows) {
      return $this->responseInvokeCommand('removeClass');
    }
  }

  /**
   * Create Ajax command for adding or removing 'd-block' CSS class.
   *
   * @param string $method
   *   'addClass' or 'removeClass' jQuery method.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   InvokeCommand Ajax command.
   */
  private function responseInvokeCommand(string $method) {
    $response = new AjaxResponse();

    $selector = '.logo .fa-exclamation-circle';
    $arguments = ['d-block'];

    $response->addCommand(new InvokeCommand($selector, $method, $arguments));
    return $response;
  }

}