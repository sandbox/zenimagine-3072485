(function ($, Drupal) {

  'use strict';

  $(document).ready(function() {
    $(".nav-link").click(function() {
      if ($(this)[0].id == 'pills-private-tab') {
        $(".pillsPrivateClass").each(function() {
          if ($(this).hasClass('active')) { /* */ } else {
            $(this).addClass('active');
            $(this).addClass('show');
          }
        });
        $(".pillsPublicClass").each(function() {
          if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).removeClass('show');
          }
        });
      } else {
        $(".pillsPrivateClass").each(function() {
          if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).removeClass('show');
          }
        });
      }
    });
  });

})(jQuery, Drupal);