/**
 * @file
 * Periodically sends Ajax requests to the controller to check if a new message arrived.
 * Send request only if 'd-block' CSS class is removed from the logo (to decrease server load).
 */

(function ($, Drupal, drupalSettings) {

  // If the front page is reloaded, then contains 1, otherwise 0.
  var frontPageReloaded = drupalSettings.nmn.frontPageReloaded;
  // Flag for disable Ajax requests when the current browser tab lose focus.
  var browser_tab_is_active = true;

  // Handle tab visibility change events.
  function handleVisibilityChange() {
    if (document.visibilityState == "hidden") {
      browser_tab_is_active = false;
    } else {
      browser_tab_is_active = true;
      $('.logo').removeClass('b-block');
      request();
    }
  }

  document.addEventListener('visibilitychange', handleVisibilityChange, false);

  // If a user reloaded the front page, then remove 'd-block' CSS class.
  // It is assumed that a user see new messages here (if they was arrived).
  if (frontPageReloaded) {
    $('.logo').removeClass('d-block');
    frontPageReloaded = false;
  }

  function request() {
    var ajax = new Drupal.Ajax(false, false, {
      url: Drupal.url('check-new-message/')
    });

    if (browser_tab_is_active) {
      // Time delay for Ajax requests in ms.
      var delay = 10000;
 	    ajax.execute()
        .done(function() { setTimeout(request, delay); })
        .fail(function() { setTimeout(request, delay); });
    }
  }

  request();

})(jQuery, Drupal, drupalSettings);